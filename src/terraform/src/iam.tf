resource "google_service_account" "challenge-rw-user" {
  account_id   = "${var.project_id}-rw-user"
  display_name = "Usuario de full admin"
}

resource "google_project_iam_binding" "challenge-rw-user_gke" {
  project = var.project_id
  role    = "roles/container.admin"
  
  members = [
    "serviceAccount:${google_service_account.challenge-rw-user.email}",
  ]
}

resource "google_project_iam_binding" "challenge-rw-user_storage" {
  project = var.project_id
  role    = "roles/storage.admin"
  
  members = [
    "serviceAccount:${google_service_account.challenge-rw-user.email}",
  ]
}

resource "google_project_iam_binding" "challenge-rw-user_mysql" {
  project = var.project_id
  role    = "roles/cloudsql.client"
  
  members = [
    "serviceAccount:${google_service_account.challenge-rw-user.email}",
  ]
}

resource "google_service_account_key" "challenge-rw-user_key" {
  service_account_id = google_service_account.challenge-rw-user.name
  private_key_type   = "json"
}

#######

resource "google_service_account" "challenge-ro-user" {
  account_id   = "${var.project_id}-ro-user"
  display_name = "Usuario de solo Lectura"
}

resource "google_project_iam_binding" "challenge-ro-user_gke" {
  project = var.project_id
  role    = "roles/container.viewer"
  
  members = [
    "serviceAccount:${google_service_account.challenge-ro-user.email}",
  ]
}

resource "google_project_iam_binding" "challenge-ro-user_storage" {
  project = var.project_id
  role    = "roles/storage.objectViewer"
  
  members = [
    "serviceAccount:${google_service_account.challenge-ro-user.email}",
  ]
}

resource "google_project_iam_binding" "challenge-ro-user_mysql" {
  project = var.project_id
  role    = "roles/cloudsql.viewer"
  
  members = [
    "serviceAccount:${google_service_account.challenge-ro-user.email}",
  ]
}

resource "google_service_account_key" "challenge-ro-user_key" {
  service_account_id = google_service_account.challenge-ro-user.name
  private_key_type   = "json"
}

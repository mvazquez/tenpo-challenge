resource "google_storage_bucket" "tenpo-challenge" {
  name          = "${var.project_id}-bucket"
  location      = "US"
  force_destroy = true
  storage_class = "STANDARD"
  uniform_bucket_level_access = true
  versioning {
    enabled = true
  }
  lifecycle {
    prevent_destroy = true
  }
  labels = {
    environment = "tenpo-challenge"
  }
}

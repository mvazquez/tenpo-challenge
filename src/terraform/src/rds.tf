resource "google_sql_database_instance" "challenge-sql" {
  name             = "${var.project_id}-mysql"
  database_version = "MYSQL_5_7"
  region           = var.region
  settings {
    tier = "db-f1-micro"
  }
}
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.74.0"
    }
  }

  required_version = ">= 0.14"
}

provider "google" {
  credentials = file("<path_alarchivo_de_credenciales_json>")
  project     = var.project_id
  region      = var.region
}

variable "gke_num_nodes" {
  default     = 2
  description = "number of gke nodes"
}

data "google_container_engine_versions" "gke_version" {
  location = var.region
  version_prefix = "1.27."
}

resource "google_container_cluster" "challenge" {
  name     = "${var.project_id}-gke"
  location = var.region
  remove_default_node_pool = true
  initial_node_count       = 1
  enable_legacy_abac       = false
  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }
  master_authorized_networks_config {
    cidr_blocks {
      cidr_block ="10.10.10.10/0"
      display_name = "challenge-authorized-net"
    }
  }
  authenticator_groups_config{
    security_group="gke-security-groups@tenpo.cl"
  }
  pod_security_policy_config {
    enabled = true
  }
  release_channel {
    channel = var.release_channel
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${var.project_id}-preemptible-nodes"
  location   = var.region
  cluster    = google_container_cluster.challenge.name
  version    = data.google_container_engine_versions.gke_version.release_channel_latest_version["STABLE"]
  node_count = var.gke_num_nodes
  node_config {
    preemptible  = true
    machine_type = "e2-medium"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata     = {
      disable-legacy-endpoints = "true"
    }
    oauth_scopes    = [
        "https://www.googleapis.com/auth/cloud-platform",
        "https://www.googleapis.com/auth/logging.write",
        "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
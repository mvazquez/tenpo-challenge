# Challenge 2: Infraestructura como código (Terraform)

## Objetivo

Crear y administrar recursos en la nube utilizando infraestructura como código.

## Requisitos

* Un recurso de almacenamiento genérico (Azure Storage Account, S3, GCS, etc.)
* Un cluster Kubernetes (AKS, GKE, EKS)
* Una Base de datos relacional
* Un usuario/cuenta con acceso de lectura sobre los recursos creados
* Un usuario/cuenta de acceso administrador sobre los recursos creados
* Cree un Pipeline que implemente el código del repositorio de Git en la nube elegida implementando algunas pruebas sobre el código terraform (linting, checkov, tfsec, etc)

## Respuestas y comentarios

    IMPORTANTE:
        * Se debe tener instalado Terraform.
        * Se debe tener configuraco el acceso a la nube elegida configurando el provider correspondiente.

* Un recurso de almacenamiento genérico (Azure Storage Account, S3, GCS, etc.)

    Para este punto se creó un bucket en GCP Bucket. El código se encuentra en el archivo [s3.tf](src/s3.tf).

* Un cluster Kubernetes (AKS, GKE, EKS)
    
    Para este punto se creó un cluster en GCP GKE. El código se encuentra en el archivo [gke.tf](src/gke.tf).

* Una Base de datos relacional
    
    Para este punto se creó una base de datos en AWS RDS. El código se encuentra en el archivo [rds.tf](src/rds.tf).

* Un usuario/cuenta con acceso de lectura sobre los recursos creados
        
    Para este punto se creó un usuario con permisos de lectura sobre los recursos creados. El código se encuentra en el archivo [iam.tf](iam.tf).

* Un usuario/cuenta de acceso administrador sobre los recursos creados
        
    Para este punto se creó un usuario con permisos de administrador sobre los recursos creados. El código se encuentra en el archivo [iam.tf](iam.tf).

* Cree un Pipeline que implemente el código del repositorio de Git en la nube elegida implementando algunas pruebas sobre el código terraform (linting, checkov, tfsec, etc)

    Para este punto se creó un pipeline en GitlabCI que implementa los linter correspondiente.

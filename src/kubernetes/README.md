# Challenge 1: Kubernetes, Orquestador de Contenedores

## Objetivo

Crear y administrar aplicaciones utilizando Kubernetes como orquestador de
contenedores.

## Requisitos

* Ejecute un servicio web que responde a solicitudes HTTP (siéntase libre de usar cualquier imagen Docker existente o cree la suya propia)
* Configure su aplicación para servir en 443 apuntando a 80 (Deseable)
* Use un certificado autofirmado para habilitar SSL (Deseable)
* Configure el escalado automático de su aplicación usando la CPU como indicador con mínimo 2 y máximo 10 réplicas
* Cree un archivo de configuración de Kubernetes que podamos aplicar a un clúster existente usando kubectl

## Respuestas y comentarios

    IMPORTANTE: Se parte del presupuesto que en el cluster se encuentra instalado y funcionando los siguientes controllers:
        * Metrics-Server (Para el funcionamiento del HPA)
        * Traefik Ingress-Controller (Puede ser cualquier otro, pero se debe cambiar el manifiesto del Ingress)
            * https://doc.traefik.io/traefik/v2.3/getting-started/install-traefik/#use-the-helm-chart
        * Cert-Manager (Para el funcionamiento del secret con el certificado autofirmado)
            * https://cert-manager.io/docs/installation/helm/
        * Dominio funcional y apuntando al servicio de LoadBalancer del Ingress-Controller (Para el funcionamiento del Ingress y el certificado autofirmado vía Let's Encrypt)

* Ejecute un servicio web que responde a solicitudes HTTP (siéntase libre de usar cualquier imagen Docker existente o cree la suya propia)

    Se utiliza el servicio `whoami` provisto por `traefik` que se encuentra disponible en dockerhub para este punto. El deployment se encuentra en el archivo [challenge-deployment.yaml](challenge-deployment.yaml).

* Configure su aplicación para servir en 443 apuntando a 80 (Deseable)
    
    Para este punto se creó un Ingress que redirige el tráfico del puerto 443 al 80 del servicio.
    Tambien se hace la aclaración que el servicio (`deployment`) se ejecuta adrede en el puerto 8080 ya que dependiendo
    de como se encuentre configurado el Cluster, puede que las constrains de seguridad no permitan ejecutar un servicio en un puerto menor a 1024. El Ingress se encuentra en el archivo [challenge-deployment.yaml](challenge-deployment.yaml).

* Use un certificado autofirmado para habilitar SSL (Deseable)

    Para este punto se partede la premisa que ya se encuentra instalado y funcionando el controller `cert-manager` en el cluster junto con el Ingress-Controller `traefik`, para que ambos puedan funcionar en conjunto y poder obtener un certificado autofirmado vía Let's Encrypt. La configuración para la obtención del certificado se puede encontrar en el archivo [challenge-deployment.yaml](challenge-deployment.yaml) bajo los `apiVersion` de `cert-manager.io/v1`.

* Configure el escalado automático de su aplicación usando la CPU como indicador con mínimo 2 y máximo 10 réplicas

    Para este punto se creó un HPA (Horizontal Pod Autoscaler) que escala el deployment creado anteriormente. El HPA se encuentra en el archivo [challenge-deployment.yaml](challenge-deployment.yaml) bajo los `apiVersion` de `autoscaling/v2`.
    Recordar que para que este funcione correctamente debe estar instalado y funcionando el controller `metrics-server` en el cluster.

* Cree un archivo de configuración de Kubernetes que podamos aplicar a un clúster existente usando kubectl

    Para este punto se creó un archivo [challenge-deployment](challenge-deployment.yaml) que contiene los manifiesto de configuración de los puntos anteriores.
# Challenge 3: Scripting, Automatización

## Objetivo

Escrir un procedimiento para procesar el archivo adjunto [clientes.csv](data/clientes.csv)

## Requisitos

* Filtre solo a los clientes que viven en la ciudad “Santiago”
* El resultado debe ser una lista de clientes con siguiente formato: Nombre completo seguido de la dirección de correo electrónico entre <>: "Nombre Apellido <correo>" , ejemplo: Pedro Perez <pedro@ejemplo.com>
* Ordene el resultado en orden alfabético por nombre del cliente
* Utilice alguno de estos lenguajes de programación: NodeJS, Go, Bash, Python
* Cree una imagen de Docker que ejecute el procedimiento anterior

## Respuestas y comentarios

* Para llevar a cabo el pedido, se realizo un script en python utilizando la libreria pandas. El script se encuentra en el archivo [csvParser.py](src/csvParser.py).

* Se ralizo un Dockerfile para crear una imagen de Docker que ejecute el script anterior. El Dockerfile se encuentra en el archivo [Dockerfile](Dockerfile).

* Se agrego un job en la CI para buildear la imagen y publicarla en la registry del proyecto
    > registry.gitlab.com/mvazquez/tenpo-challenge/challenge03

* Se creo un `docker-compose` para facilitar la prueba y ejecución de la imágen y el script; montando el archivo csv del challenge en la imagen y pasandola como parametro del entrypoint para que el script la levante. El archivo se encuentra en [docker-compose.yml](docker-compose.yml)
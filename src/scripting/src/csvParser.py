#!/usr/bin/env python

import sys
import pandas as pd

if len(sys.argv) != 2:
    print("Use: csvParser.py <fullpath_to_csv-file>")
    sys.exit(1)

csvFile = sys.argv[1]

try:
    # Cargar el archivo CSV en un DataFrame
    df = pd.read_csv(csvFile)
except FileNotFoundError:
    print(f"CSV Not found: {csvFile}")
    sys.exit(1)

# Agregar una columna 'Full_Name' al DataFrame original
df.loc[df['Ciudad'] == 'Santiago', 'Full_Name'] = df['Nombre'] + ' ' + df['Apellido'] + ' <' + df['Email'] + '>'

# Filtramos el DataFrame por la columna 'Ciudad' y el valor 'Santiago'
clientSantiago = df[df['Ciudad'] == 'Santiago']

# imprimimos el resultado
print(clientSantiago.sort_values(by='Full_Name')["Full_Name"].to_string(index=False))
# Challenge Tenpo: DevOps v1.0

Desafío de DevOps para postulantes a Tenpo.

---

## Introducción

Este es un repositorio enfocado a la resolcuión del desafío de DevOps para postulantes a Tenpo.</br>
El mismo consta de tres partes:

* [Challenge 1: Kubernetes, Orquestador de Contenedores](src/kubernetes/README.md)
* [Challenge 2: Infraestructura como código (Terraform)](src/terraform/README.md)
* [Challenge 3: Scripting, Automatización](src/scripting/README.md)

En cada uno de ellos se explica el objetivo, los requisitos y las respuestas/comentarios, junto con los archivos necesarios para la resolución del desafío.
